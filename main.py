#!/usr/bin/env python

from psycopg2 import connect

from parser.npm.outdated import OutdatedParser as NpmOutdatedParser
from parser.npm.security import SecurityParser as NpmSecurityParser
from parser.yarn.outdated import OutdatedParser as YarnOutdatedParser
from parser.yarn.security import SecurityParser as YarnSecurityParser
from parser.composer.outdated import OutdatedParser as ComposerOutdatedParser
from parser.composer.security import SecurityParser as ComposerSecurityParser

from time import gmtime, strftime

import argparse


class BColors:
    HEADER = '\033[95m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


ap = argparse.ArgumentParser()
ap.add_argument("-p", "--project", required=True, help="Project Folder")
ap.add_argument("--name", required=True, help="Project Name")
ap.add_argument("--rs_db_name", required=False, help="Redshift Database Name", default="dwh")
ap.add_argument("--rs_host", required=True, help="Redshift Hostname")
ap.add_argument("--rs_port", required=False, help="Redshift Port", default=5439)
ap.add_argument("--rs_user", required=True, help="Redshift User")
ap.add_argument("--rs_password", required=True, help="Redshift Password")
ap.add_argument("-y", "--yarn-audit", required=False, help="Run yarn audit", action="store_true")
ap.add_argument("-n", "--npm-audit", required=False, help="Run npm audit", action="store_true")
ap.add_argument("-c", "--composer-audit", required=False, help="Run composer audit", action="store_true")
ap.add_argument("--npm-bin", required=False, help="Path to npm binary", default="npm")
ap.add_argument("--yarn-bin", required=False, help="Path to yarn binary", default="yarn")
ap.add_argument("--composer-bin", required=False, help="Path to composer binary", default="composer")
ap.add_argument("--symfony-bin", required=False, help="Path to symfony CLI binary", default="symfony")
args = vars(ap.parse_args())


def main():
    dependency_type = None
    outdated = (0,) * 3     # Patches, Minor, Major1
    security_issues = (0,) * 5  # Info, Low, Moderate, High, Critical

    if args['npm_audit']:
        dependency_type = 'npm'
        outdated_parser = NpmOutdatedParser(args['npm_bin'], args['project'])
        security_parser = NpmSecurityParser(args['npm_bin'], args['project'])
        outdated = add_tuples(outdated, outdated_parser.gather_data())
        security_issues = add_tuples(security_issues, security_parser.gather_data())

    elif args['yarn_audit']:
        dependency_type = 'yarn'
        outdated_parser = YarnOutdatedParser(args['yarn_bin'], args['project'])
        security_parser = YarnSecurityParser(args['yarn_bin'], args['project'])
        outdated = add_tuples(outdated, outdated_parser.gather_data())
        security_issues = add_tuples(security_issues, security_parser.gather_data())

    elif args['composer_audit']:
        dependency_type = 'composer'
        outdated_parser = ComposerOutdatedParser(args['composer_bin'], args['project'])
        security_parser = ComposerSecurityParser(args['symfony_bin'], args['project'])
        outdated = add_tuples(outdated, outdated_parser.gather_data())
        security_issues = add_tuples(security_issues, security_parser.gather_data())

    if dependency_type is None:
        raise ValueError("Please provide one type of dependency to check for")

    if args['rs_db_name'] and args['rs_host'] and args['rs_port'] and args['rs_user'] and args['rs_password']:
        connection = connect(
                dbname=args['rs_db_name'],
                host=args['rs_host'],
                port=args['rs_port'],
                user=args['rs_user'],
                password=args['rs_password']
            )
        cursor = connection.cursor()

        cursor.execute(
            "INSERT INTO xdevelopment.dependencies (project, dependency_type, date, outdated_patch, outdated_minor, " +
            "outdated_major, security_info, security_low, security_moderate, security_high, security_critical) " +
            "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            (args['name'], dependency_type, strftime("%Y-%m-%d %H:%M:%S", gmtime()), *outdated, *security_issues)
        )
        cursor.close()
        connection.commit()
        connection.close()

    print("Outdated: " + BColors.GREEN + f"{outdated[0]} Patches, " + BColors.WARNING + f"{outdated[1]} Minors, "
          + BColors.FAIL + f"{outdated[2]} Majors" + BColors.ENDC)
    print("Security Issues: " + BColors.BLUE + f"{security_issues[0]} Info, " + BColors.GREEN
          + f"{security_issues[1]} Low, " + BColors.WARNING + f"{security_issues[2]} Moderate, " + BColors.FAIL
          + f"{security_issues[3]} High, " + BColors.BOLD + f"{security_issues[4]} Critical" + BColors.ENDC)


def add_tuples(a, b):
    return tuple(map(sum, zip(a, b)))


if __name__ == '__main__':
    main()
