# Dependency Scan

This project runs `yarn|npm audit`, `yarn|npm outdated`, `composer outdated` and `symfony security:check`
and inserts the result into Redshift

## KPIs

Using yarn and composer this tool checks for the following KPIs:

- Outdated Patches: Number of dependencies where patch version differs from the latest version
- Outdated Minor: Number of dependencies where minor version differs from the latest version
- Outdated Major: Number of dependencies where major version differs from the latest version
- Security Issues Info: Number of security issues with a info level
- Security Issues Low: Number of security issues with a low level
- Security Issues Moderate: Number of security issues with a moderate level
- Security Issues High: Number of security issues with a high level
- Security Issues Critical: Number of security issues with a critical level

## Project Setup

Make sure you have [Pipenv](https://pipenv.readthedocs.io/en/latest/) installed.

Then run `pipenv install`. To execute the `main.py` script you can either run `pipenv run python main.py` or run `pipenv shell` to load the pipenv environment into your shell.

## Usage

The redshift table used to store the results has the following columns:
> Id, Project, Date, Outdated Patches, Outdated Minor, Outdated Major, Security Issues Info, Security Issues Low, Security Issues Moderate, Security Issues High, Security Issues Critical

```
./dependency-scan -n <PROJECT_NAME> -p <path> -y -c --rs_host <REDSHIFT_HOSTNAME> --rs_user <REDSHIFT_USERNAME> --rs_password <REDSHIFT_PASSWORD>
```

### Docker

To use the docker container `wbgrs/dependency-scan` run the following command and add the parameters you need.

```
docker run \
   --mount type=bind,source="$(pwd)"/,target=/app \
   --mount type=bind,source="$(pwd)"/credentials.json,target=/script/credentials.json \
   --mount type=bind,source="$(pwd)"/token.json,target=/script/token.json \
   wbgrs/dependency-scan \
   pipenv run python main.py -p /app
```

#### Build the Docker Container

Check the [docker repository](https://gitlab.com/webgears-gmbh/docker) to build new docker images there.

### Arguments

```
-p|--project <Path>         Project Folder to check (Required)
-n|--name <Name>            Project Name (Required)
--rs_db_name <DB Name>      Redshift Database Name (Optional)
--rs_host <Hostname>        Redshift Hostname (Required)
--rs_port <Port>            Redshift Port (Optional)
--rs_usere <User>           Redshift Username (Required)
--rs_password <Password>    Redshift Password (Required)
-y|--yarn-audit             Run yarn audit (Optional)
-n|--npm-audit              Run npm audit (Optional)
-c|--composer-audit         Run composer audit (Optional)
--yarn-bin                  Path to yarn binary (Optional, defaults to "yarn")
--npm-bin                   Path to npm binary (Optional, defaults to "npm")
--composer-bin              Path to composer binary (Optional, defaults to "composer")
--symfony-bin      Path to symfony CLI binary (Optional, defaults to "symfony")
```

## Build

Run `pyinstaller --onefile -n dependency-scan main.py`. This will build the executeable for your machine.
For more info please read the info on [PyInstaller](https://www.pyinstaller.org/).

## Troubleshooting

> UserWarning: Cannot access token.json: No such file or directory

Please create an empty `token.json` file in the folder of the executable.



